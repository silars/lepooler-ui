FROM node:12.18.0 AS compile-image

# Get package.json and install modules
COPY package.json /tmp/package.json
# TODO : Remove the --legacy-peer-deps parameter after migrating away from ng2-tooltip-directive
RUN cd /tmp && npm install --legacy-peer-deps
RUN mkdir -p /app && cp -a /tmp/node_modules /app/

WORKDIR /app
COPY . /app
RUN npm run build-deploy

FROM nginx
COPY nginx.conf /usr/local/nginx/conf
COPY run.sh /
COPY --from=compile-image /app/dist/lepooler-ui /usr/share/nginx/html
EXPOSE 80
ENTRYPOINT [ "sh", "/run.sh"]