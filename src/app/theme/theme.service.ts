import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ThemeService {
  private static THEME_KEY = 'theme';
  private static LIGHT_THEME_NAME = 'flatly';
  private static DARK_THEME_NAME = 'darkly';
  private static readonly THEME_FRIENDLY_NAMES = new Map<string, string>([
    [ThemeService.LIGHT_THEME_NAME, 'light'],
    [ThemeService.DARK_THEME_NAME, 'dark'],
  ]);

  private currentTheme: string;

  public themeChanged = new BehaviorSubject<string>(undefined);

  constructor() {
    this.currentTheme = localStorage.getItem(ThemeService.THEME_KEY);
    if (!this.currentTheme) {
      this.setDarkTheme();
    } else {
      this.loadCurrentTheme();
    }
  }

  public getCurrentTheme(): string {
    return ThemeService.THEME_FRIENDLY_NAMES.get(this.currentTheme);
  }

  public setLightTheme(): void {
    this.currentTheme = ThemeService.LIGHT_THEME_NAME;
    this.saveAndLoadCurrentTheme();
  }

  public setDarkTheme(): void {
    this.currentTheme = ThemeService.DARK_THEME_NAME;
    this.saveAndLoadCurrentTheme();
  }

  private saveAndLoadCurrentTheme(): void {
    this.saveCurrentTheme();
    this.loadCurrentTheme();
  }

  private saveCurrentTheme(): void {
    localStorage.setItem(ThemeService.THEME_KEY, this.currentTheme);
  }

  public loadCurrentTheme(): void {
    const html = document.getElementsByTagName('html')[0];
    html.className = this.currentTheme;
    this.themeChanged.next(this.currentTheme);
  }
}
