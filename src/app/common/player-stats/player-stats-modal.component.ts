import { Component, Input, OnChanges } from '@angular/core';

import { Player } from '../../models/player';

@Component({
    selector: 'app-player-stats-modal',
    templateUrl: './player-stats-modal.component.html'
})
export class PlayerStatsModalComponent implements OnChanges {
    @Input() public player: Player;

    public playerName: string;
    public playerStatsUrl: string;

    ngOnChanges() {
        this.playerName = this.player.name;
        this.playerStatsUrl = `https://www.nhl.com/player/${this.player.playerNhlId}`;
    }
}
