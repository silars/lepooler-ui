import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PoolerPicksComponent } from './pooler-picks.component';

describe('PoolerPicksComponent', () => {
    let component: PoolerPicksComponent;
    let fixture: ComponentFixture<PoolerPicksComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [ PoolerPicksComponent ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PoolerPicksComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
