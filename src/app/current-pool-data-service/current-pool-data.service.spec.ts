import { TestBed, inject } from '@angular/core/testing';

import { CurrentPoolDataService } from './current-pool-data.service';

describe('CurrentPoolDataService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [CurrentPoolDataService]
        });
    });

    it('should be created', inject([CurrentPoolDataService], (service: CurrentPoolDataService) => {
        expect(service).toBeTruthy();
    }));
});
