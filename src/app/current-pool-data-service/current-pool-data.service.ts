import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { Pool } from '../models/pool';

@Injectable({
  providedIn: 'root',
})
export class CurrentPoolDataService {
  private currentPool = new Subject<Pool>();

  public currentPool$ = this.currentPool.asObservable();
  public isSpectating = false;

  public setCurrentPool(pool: Pool, isSpectating: boolean): void {
    this.isSpectating = isSpectating;
    this.currentPool.next(pool);
  }
}
