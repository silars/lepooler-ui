import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { environment } from '../../../environments/environment';

@Injectable()
export class AuthGuard implements CanActivate {
    protected cannotActivateMessage = 'Vous devez d\'abord créer un pool ou vous authentifier';

    constructor(private router: Router, private toastr: ToastrService) { }

    public canActivate(): boolean {
        if (localStorage.getItem(environment.tokenStorageName)) {
            return true;
        }
        return this.authenticationRequired();
    }

    protected authenticationRequired(): boolean {
        this.router.navigate(['/landing']);
        this.toastr.error(this.cannotActivateMessage);
        return false;
    }
}
