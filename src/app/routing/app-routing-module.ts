import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LandingComponent } from '../landing/landing.component';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { PlayerSelectionReportComponent } from '../player-selection-report/player-selection-report.component';
import { PlayerSelectionComponent } from '../player-selection/player-selection.component';
import { AuthGuard } from './guards/auth-guard-service';
import { PlayerSelectionCompleteGuard } from './guards/player-selection-complete-guard-service';
import { SpectatingAuthGuard } from './guards/spectating-auth-guard-service';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'landing',
        pathMatch: 'full'
    },
    {
        path: 'landing',
        component: LandingComponent
    },
    {
        path: 'spectate/:poolGuid',
        component: LandingComponent,
        data : { isSpectating: true }
    },
    {
        path: 'spectate',
        canActivate: [SpectatingAuthGuard],
        component: PlayerSelectionComponent,
        data : { isSpectating: true }
    },
    {
        path: 'spectateReport',
        canActivate: [SpectatingAuthGuard],
        component: PlayerSelectionReportComponent,
        data : { isSpectating: true }
    },
    {
        path: 'playerSelection',
        canActivate: [AuthGuard],
        component: PlayerSelectionComponent
    },
    {
        path: 'report',
        canActivate: [AuthGuard, PlayerSelectionCompleteGuard],
        component: PlayerSelectionReportComponent
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true, relativeLinkResolution: 'legacy' })],
    exports: [RouterModule],
    providers: []
})
export class AppRoutingModule {
}
