import { HttpErrorResponse } from '@angular/common/http';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Howl } from 'howler';
import { ToastrService } from 'ngx-toastr';
import { combineLatest, Subscription } from 'rxjs';

import { KeyValuePair } from '../common/key-value-pair';
import { CurrentPoolDataService } from '../current-pool-data-service/current-pool-data.service';
import { Player } from '../models/player';
import { Pool } from '../models/pool';
import { PoolParticipant } from '../models/pool-participant';
import { PoolerPicksUpdate } from '../models/spectating/pooler-picks-update';
import { PoolService } from '../pool-service/pool.service';
import { AddNewPoolerComponent } from './add-new-pooler/add-new-pooler.component';
import { PlayerSelectionControlsComponent } from './player-selection-controls/player-selection-controls.component';
import { PlayerSelectionDataService } from './player-selection-data-service/player-selection-data.service';
import { PlayersGridComponent } from './players-grid/players-grid.component';
import { PoolersListComponent } from './poolers-list/poolers-list.component';

@Component({
  selector: 'app-player-selection',
  templateUrl: './player-selection.component.html',
})
export class PlayerSelectionComponent implements OnInit, AfterViewInit, OnDestroy {
  public playerSelectionStarted = false;
  public playerSelectionCompleted = false;
  public currentPool: Pool = new Pool({});
  public guestUrl: string;
  public isSpectating = false;

  private poolParticipants: PoolParticipant[];
  private players: Player[];

  private routeSnapshot: ActivatedRouteSnapshot;

  @ViewChild(PoolersListComponent) private poolParticipantsList: PoolersListComponent;
  @ViewChild(AddNewPoolerComponent) private addNewPoolerComponent: AddNewPoolerComponent;
  @ViewChild(PlayerSelectionControlsComponent) private playerSelectionControls: PlayerSelectionControlsComponent;
  @ViewChild(PlayersGridComponent) private playersGrid: PlayersGridComponent;

  private subscriptions: Subscription[] = [];

  public constructor(
    private route: ActivatedRoute,
    private poolService: PoolService,
    private playerSelectionDataService: PlayerSelectionDataService,
    private currentPoolDataService: CurrentPoolDataService,
    private toastr: ToastrService
  ) {
    this.routeSnapshot = this.route.snapshot;
  }

  ngOnInit() {
    this.isSpectating = this.routeSnapshot.data.isSpectating || this.currentPoolDataService.isSpectating;

    this.poolService.connectToSpectateHub();
    if (this.isSpectating) {
      this.subscriptions.push(
        this.poolService.spectatePoolParticipants$.subscribe((poolParticipants: PoolParticipant[]) => {
          this.playerSelectionDataService.updatePoolersMap(poolParticipants);
        })
      );

      this.subscriptions.push(
        this.poolService.spectatePicks$.subscribe((poolerPicksUpdate: PoolerPicksUpdate) => {
          this.handlePickSuccess(
            {
              key: poolerPicksUpdate.poolerGuid,
              value: poolerPicksUpdate.poolerPicks,
            },
            poolerPicksUpdate.pickedPlayerId
          );
        })
      );

      this.subscriptions.push(
        this.poolService.spectatePicksCompleted$.subscribe((picksCompleted: boolean) => {
          this.playerSelectionCompleted = picksCompleted;
          if (picksCompleted) {
            this.picksCompleted();
          }
        })
      );
    }

    this.subscriptions.push(
      this.playerSelectionDataService.poolParticipants$.subscribe((poolParticipants: PoolParticipant[]) => {
        this.poolParticipants = poolParticipants;
        this.updateCurrentRound();
      })
    );

    this.subscriptions.push(
      this.poolService.getPoolInfo().subscribe((currentPool: Pool) => {
        this.currentPoolDataService.setCurrentPool(currentPool, this.isSpectating || this.currentPoolDataService.isSpectating);
        this.currentPool = currentPool;
        const port = location.port ? `:${location.port}` : '';
        this.guestUrl = `${location.protocol}//${location.hostname}${port}/${window.location.pathname}#/spectate/${this.currentPool.poolGuid}`;
        this.getPlayersForDraft();
      })
    );
  }

  public ngAfterViewInit(): void {
    this.subscriptions.push(
      this.playerSelectionDataService.players$.subscribe((players: Player[]) => {
        this.players = players;
        this.updateCurrentRound();
        this.refresh();
      })
    );
  }

  public ngOnDestroy(): void {
    this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  private getPlayersForDraft(): void {
    this.subscriptions.push(
      combineLatest([
        this.poolService.getPlayersPredictions(this.currentPool.season),
        this.poolService.getPoolParticipantsWithPickOrder(),
        this.poolService.getAllPicksInPool(),
      ]).subscribe(([players, poolParticipants, picks]) => {
        this.playerSelectionDataService.updateData(players, poolParticipants, picks);
        this.updateCurrentRound();
      })
    );
  }

  public pickPlayer(playerToPick: Player, attemptNumber = 1): void {
    if (this.playerSelectionStarted) {
      this.playerSelectionControls.stopCountdown();
    }
    this.subscriptions.push(
      this.poolService.pickPlayer(playerToPick.playerNhlId).subscribe(
        (poolerPicks: KeyValuePair<string, Player[]>) => {
          this.playerSelectionStarted = true;
          this.handlePickCompleted(true, poolerPicks, playerToPick.playerNhlId);
        },
        (error: HttpErrorResponse) => {
          if (error.status === 409) {
            this.handlePickCompleted(false);
            this.playerSelectionControls.resumeCountdown();
          } else {
            if (attemptNumber < 3) {
              this.pickPlayer(playerToPick, attemptNumber + 1);
            } else {
              this.handlePickCompleted(false);
              this.toastr.error('Plus de 3 échecs de la sélection. Le décompte recommencera une fois que le choix aura été effectué avec succès.');
            }
          }
        }
      )
    );
  }

  public forcePickBestPlayer(playersInRankOrder: Player[] = [], attemptNumber = 1): void {
    this.playersGrid.gridIsLoading();
    if (playersInRankOrder.length === 0) {
      playersInRankOrder = this.players.filter((p: Player) => !p.picked).sort((a: Player, b: Player) => a.predictions[0].rank - b.predictions[0].rank);
    }
    const bestPlayerId = playersInRankOrder[0].playerNhlId;
    this.subscriptions.push(
      this.poolService.pickPlayer(bestPlayerId).subscribe(
        (poolerPicks: KeyValuePair<string, Player[]>) => {
          this.handlePickCompleted(true, poolerPicks, bestPlayerId);
        },
        (error: HttpErrorResponse) => {
          if (error.status === 409) {
            // Only valid case for a conflict response for a force pick is that the pooler being force picked already
            // has the maximum number of players at the position of the force picked player.
            if (attemptNumber < 2) {
              const currentPlayerPosition = playersInRankOrder.find((p: Player) => p.playerNhlId === bestPlayerId).position;
              const positionsToFind = currentPlayerPosition.charAt(0) === 'G' ? ['A', 'D'] : ['G'];
              const bestPlayerWithCorrectPosition = playersInRankOrder.find((p: Player) => positionsToFind.indexOf(p.position.charAt(0)) > -1);
              this.forcePickBestPlayer([bestPlayerWithCorrectPosition], attemptNumber + 1);
            } else {
              this.handlePickCompleted(false);
            }
          } else {
            if (attemptNumber < 3) {
              this.forcePickBestPlayer(playersInRankOrder, attemptNumber + 1);
            } else {
              this.handlePickCompleted(false);
              this.toastr.error('Plus de 3 échecs de la sélection automatique. Le décompte recommencera une fois que le choix aura été effectué avec succès.');
            }
          }
        }
      )
    );
  }

  public undoLastPick(): void {
    this.playersGrid.gridIsLoading();

    this.subscriptions.push(
      this.poolService.undoLastPick().subscribe(
        (poolerPicks: KeyValuePair<string, Player[]>) => {
          const poolerPreviousPicks = this.poolParticipants.find((poolParticipant: PoolParticipant) => poolParticipant.pooler.poolerGuid === poolerPicks.key).picks;
          const removedPlayerId = poolerPreviousPicks.filter((p1: Player) => !poolerPicks.value.find((p2: Player) => p1.playerNhlId === p2.playerNhlId))[0].playerNhlId;
          this.handlePickCompleted(true, poolerPicks, removedPlayerId);
          this.playerSelectionCompleted = false;
        },
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        (error: any) => {
          this.handlePickCompleted(false);
        }
      )
    );
  }

  private handlePickCompleted(success: boolean, poolerPicks?: KeyValuePair<string, Player[]>, pickedPlayerId?: string): void {
    this.playersGrid.gridDoneLoading();
    if (success) {
      this.handlePickSuccess(poolerPicks, pickedPlayerId);
      this.poolService.updateSpectatorPicks(poolerPicks.key, pickedPlayerId);
    }
  }

  private handlePickSuccess(poolerPicks: KeyValuePair<string, Player[]>, pickedPlayerId: string): void {
    this.playerSelectionDataService.updatePoolerPicks(poolerPicks, pickedPlayerId);
    this.updateCurrentRound();

    // remove picked player; setTimeout is required because it takes some time for the datatable to be ready to refresh its data after a player has been marked as picked
    setTimeout(() => this.refresh(), 500);

    if (poolerPicks.value.length === 0 && this.playerSelectionStarted) {
      // first pick was undone
      this.playerSelectionStarted = false;
      this.poolParticipantsList.updatePickingPooler();
    } else {
      this.playerSelectionCompleted = false;
      this.poolParticipantsList.updatePickingPooler();
      if (this.playerSelectionControls !== undefined) {
        this.playerSelectionControls.restart();
      }
    }
  }

  public picksCompleted(): void {
    if (this.playerSelectionStarted) {
      new Howl({ src: ['assets/hockey_goal_horn.mp3'], volume: 0.25 }).play();
    }
    this.playerSelectionStarted = this.poolParticipants.length > 0;
    this.playerSelectionCompleted = true;
    if (!this.isSpectating) {
      this.poolService.notifySpectatorsPicksCompleted();
    }
  }

  private refresh(): void {
    this.playersGrid.refresh();
    Array.from(document.getElementsByTagName('tooltip')).forEach((item) => item.parentNode.removeChild(item));
  }

  private updateCurrentRound(): void {
    if (this.poolParticipants) {
      this.currentPool.totalRounds = this.currentPool.nbGoalerPicks + this.currentPool.nbOffenseDefensePicks;
      let totalPicks = 1;
      this.poolParticipants.forEach((poolParticipant: PoolParticipant) => (totalPicks += poolParticipant.picks?.length ?? 0));
      this.currentPool.currentRound = Math.ceil(totalPicks / this.poolParticipants.length);
    } else {
      this.currentPool.currentRound = 0;
    }
  }
}
