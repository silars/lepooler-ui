import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { PoolParticipant } from '../../models/pool-participant';
import { PoolService } from '../../pool-service/pool.service';
import { PlayerSelectionDataService } from '../player-selection-data-service/player-selection-data.service';

@Component({
  selector: 'app-add-new-pooler',
  templateUrl: './add-new-pooler.component.html',
})
export class AddNewPoolerComponent implements OnDestroy {
  public newPoolerName: string;

  private subscriptions: Subscription[] = [];

  public constructor(private poolService: PoolService, private playerSelectionDataService: PlayerSelectionDataService) {}

  public ngOnDestroy(): void {
    this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  public addNewPooler(newPoolerName: string): void {
    this.subscriptions.push(
      this.poolService.addPoolParticipantToPool(newPoolerName).subscribe((newPoolParticipant: PoolParticipant) => {
        this.emptyNewPoolerName();
        this.playerSelectionDataService.addPooler(newPoolParticipant);
      })
    );
  }

  private emptyNewPoolerName(): void {
    this.newPoolerName = '';
  }
}
