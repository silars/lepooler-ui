import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class PoolServiceInterceptor implements HttpInterceptor {

    constructor(private router: Router,
    private toastr: ToastrService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });

        return next.handle(request).pipe(
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            catchError((error: any, caught: Observable<HttpEvent<any>>) => {
                this.handleAuthError(error);
                return of(error);
            }));
    }

    private handleAuthError(error: HttpErrorResponse): Observable<any> {
        if (error.status === 401) {
            this.toastr.error('Veuillez vous reconnecter', 'Session expirée');
            this.router.navigate(['landing']);
            // if you've caught / handled the error, you don't want to rethrow it unless you also want downstream consumers to have to handle it as well.
            return of(error.error);
        }
        if (error.status === 404) {
            this.toastr.error('Veuillez vous connecter à un autre pool ou en créer un nouveau', 'Ce pool n\'existe plus ou a été supprimé');
            this.router.navigate(['landing']);
            // if you've caught / handled the error, you don't want to rethrow it unless you also want downstream consumers to have to handle it as well.
            return of(error.error);
        }
        if (error.status === 500) {
            // if you've caught / handled the error, you don't want to rethrow it unless you also want downstream consumers to have to handle it as well.
            return of(error.error);
        }
        if (typeof error.error === 'string') {
            this.toastr.error(error.error);
        } else {
            this.toastr.error(error.message);
        }
        throw error;
    }
}
