import { HttpClientTestingModule } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';

import { PoolService } from './pool.service';

describe('PoolService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [PoolService],
            imports: [HttpClientTestingModule]
        });
    });

    it('should be created', inject([PoolService], (service: PoolService) => {
        expect(service).toBeTruthy();
    }));
});
