import { Player } from './player';
import { Pooler } from './pooler';

export class PoolParticipant {
    constructor(value: Partial<PoolParticipant>) {
      Object.assign(this, value);
      if (!value.picks) {
        this.picks = [];
      }
    }
    
    public pooler: Pooler;
    public pickOrderPosition: number;
    public picks: Player[] = [];
}
