# LePooler-ui

Frontend for [LePooler-API](https://gitlab.com/lepooler/lepooler-api).

The application allows creating NHL pools.

## To build using the deploy configuration

ng build --prod -c=deploy

## Using Docker

The nginx reverse proxy configuration in the container is using HTTP only and it is recommended to put another reverse proxy in front of it that handles HTTPS and certificates.
### Environment variables
|Name|Required|Default <br/>(if not required)|Description|
|:-:|:-:|:-:|---|
|JWT_WHITELISTED_DOMAIN|X||Domains from which JWT tokens are allowed, in Regex format. Usually the domain in which the API is hosted. <br/>Example : `example\.com`|
|API_BASE_URL|X||Base URL (including `http://` or `https://`) for the API. <br/>Example : `https://lepoolerapi.example.com`|

### Full docker-compose example :
```
  lepooler-ui:
    container_name: lepooler-ui
    image: registry.gitlab.com/lepooler/lepooler-ui:latest
    ports:
      - 80:80
    environment:
      - JWT_WHITELISTED_DOMAIN=example\.com
      - API_BASE_URL=https://lepoolerapi.example.com
    restart: unless-stopped
```
